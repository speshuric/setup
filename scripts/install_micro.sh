#!/usr/bin/env bash
set -e

OS=$1
INSTALLDIR=${2:-"."}

function githubLatestTag {
    finalUrl=`curl https://github.com/$1/releases/latest -s -L -I -o /dev/null -w '%{url_effective}'`
    echo "${finalUrl##*v}"
}

TAG=`githubLatestTag zyedidia/micro`

echo "Downloading https://github.com/zyedidia/micro/releases/download/v$TAG/micro-$TAG-"$OS".tar.gz"
curl -L "https://github.com/zyedidia/micro/releases/download/v$TAG/micro-$TAG-"$OS".tar.gz" > micro.tar.gz

tar -xvf micro.tar.gz "micro-$TAG/micro"

rm micro.tar.gz
echo "mv micro-$TAG/micro $INSTALLDIR/micro"
mv micro-$TAG/micro $INSTALLDIR/micro
rm -rf micro-$TAG
