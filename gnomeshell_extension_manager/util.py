from enum import Enum


def print_usage():
    print("Install/remove extension from Gnome Shell Extensions site https://extensions.gnome.org/")
    print("Extension ID should be retrieved from https://extensions.gnome.org/extension/<ID>/extension-name/")
    print("Parameters are :")
    print("  --install               Install extension (default)")
    print("  --remove                Remove extension")
    print("  --user                  Installation/remove in user mode (default)")
    print("  --system                Installation/remove in system mode")
    print("  --version <version>     Gnome version (system detected by default)")
    print("  --extension-id <id>     Extension ID in Gnome Shell Extension site (compulsory)")


def check_executables():
    import shutil
    shutil.which("gnome-shell")
    shutil.which("unzip")
    shutil.which("wget")


GNOME_SITE="https://extensions.gnome.org"


# search query
# https://extensions.gnome.org/extension-query/?sort=downloads&page=1&shell_version=3.24&search=gno


class SortOrder(Enum):
    downloads = "downloads"
    recent = "recent"
    popularity = "popularity"
    name = "name"
