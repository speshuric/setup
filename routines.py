#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os
import subprocess
import shutil

sudo = shutil.which("sudo") + " "


def shexec(command):
    subprocess.call(command, shell=True)


def apt(command, params=""):
    shexec(sudo + "apt -y " + command + " " + params)


def os_update():
    print("Обновление пакетов")
    apt("update")
    apt("upgrade")
    apt("autoclean")
    apt("autoremove")
    print("Завершение обновления")


# установка git отдельно потому что он нужен для отладки и требует настройки
def git_install():
    print("Установка git")
    apt("install", "git")
    shexec("git config --global user.email speshuric@mail.ru ")
    shexec("git config --global user.name \"Alexander Speshilov\"")
    shexec("git config --global core.editor micro")
    shexec("git config --global push.default simple")
    print("Установка git завершена")


def micro_install():
    # micro:
    path = "/usr/local/bin"
    fname = path + "/micro"
    if not os.path.isfile(fname):
        shexec(sudo + "./scripts/install_micro.sh linux64 " + path)


def stuff_install():
    stuff = [
        "mc", "htop", "iotop", "synaptic", "curl",
        "gconf-editor",
        "llvm", "clang",
        "xclip",  # clipboard for micro
        # vboxguest
        "virtualbox-guest-x11-hwe",
        # other vbox:
        # virtualbox-guest-dkms, virtualbox-guest-dkms-hwe, virtualbox-guest-source, virtualbox-guest-source-hwe,
        # virtualbox-guest-utils, virtualbox-guest-utils-hwe, virtualbox-guest-x11, virtualbox-guest-x11-hwe

        "python3-pip", "python-pip"]
    apt("install", " ".join(stuff))
    micro_install()


def pip_setup():
    shexec("pip install --upgrade pip")
    shexec("pip3 install --upgrade pip")
    shexec("pip install --user GitPython")
    shexec("pip3 install --user GitPython")


def apt_select():
    # find fast mirror
    # shexec("pip3 install apt-select")

    # shexec("apt-select -C RU")

    # just copy file (sudo)
    shexec(sudo + "cp ./files/sources.list /etc/apt")


def copy_settings():
    # editor
    shutil.copy2("./files/.selected_editor", os.path.expanduser('~'))  # micro



# зафигачено по инструкции
# http://bernaerts.dyndns.org/linux/76-gnome/283-gnome-shell-install-extension-command-line-script
# http://bernaerts.dyndns.org/linux/76-gnome/345-gnome-shell-install-remove-extension-command-line-script
def gnome_install_extension(extension_id, shell_version):
    import urllib, json, tempfile #urllib2, 
    # testfile = urllib.URLopener()
    url = "https://extensions.gnome.org"
    # testfile.retrieve(url + "?pk=" + extension_id + "&shell_version=" + shell_version, "file.gz")
    response = urllib.urlopen(url + "/extension-info/?pk=" + str(extension_id) + "&shell_version=" + str(shell_version))
    data = json.loads(response.read())

    download_url = data["download_url"]
    name = data["name"]
    description = data["description"]
    uuid = data["uuid"]

    temp_zip = tempfile.mkstemp(suffix=".zip", prefix="tmp-")[1]
    shexec(
        "wget --quiet --header='Accept-Encoding:none' -O \"{0}\" \"{1}\"".format(temp_zip, url + data["download_url"]))

    extension_path = "/usr/share/gnome-shell/extensions"
    install_sudo = sudo
    # extension_path="$HOME/.local/share/gnome-shell/extensions"
    # install_sudo=""
    extension_path_uid = extension_path + "/" + uuid
    shexec(sudo + "mkdir -p {0}".format(extension_path_uid))
    shexec(sudo + "unzip -oq \"{0}\" -d {1}".format(temp_zip, extension_path_uid))
    shexec(sudo + "chmod +r {0}".format(extension_path_uid))
    os.remove(temp_zip)
    # https://wiki.gnome.org/Projects/GnomeShell/Extensions


def setup_gnome():
    # copy wallpaper 
    shexec(sudo + "cp ./files/The_Milky_Way_panorama_3.jpg /usr/share/backgrounds")
    shexec("gsettings set org.gnome.desktop.background picture-uri file:///usr/share/backgrounds/The_Milky_Way_panorama_3.jpg")

    # to save: dconf dump /org/gnome/terminal/legacy/ > gnome-terminal.conf
    # shexec("cat ./files/gnome-terminal.conf | dconf load /org/gnome/terminal/legacy/")
    # shexec("dconf load /org/gnome/terminal/ < ./files/gnome-terminal.conf")
    
    # shexec("gsettings set org.gnome.desktop.background picture-uri file:///usr/share/backgrounds/The_Milky_Way_panorama_3.jpg")

    # Есть 2 расширения dash-to-panel и dash-to-dock. Ставятся они похожим образом. Я пока не решил кто есть круче. 
    # Возможно победит новая панель от убунты
    # http://www.webupd8.org/2017/01/dash-to-panel-is-cool-icon-taskbar-for.html
    # https://github.com/jderose9/dash-to-panel
    # https://extensions.gnome.org/extension/1160/dash-to-panel/

    # https://extensions.gnome.org/extension/307/dash-to-dock/
    # https://micheleg.github.io/dash-to-dock/
    # https://github.com/micheleg/dash-to-dock

    # gnome-shell --version - вручную, пока 3.18
    shell_version = "3.18"
    extension_id = 307
    #gnome_install_extension(extension_id, shell_version)


def init():
    os_update()
    git_install()
    stuff_install()
    # plymouthd crashed with SIGSEGV - само прошло
    pip_setup()
    apt_select()
    copy_settings()

    setup_gnome()

    os_update()

# RememberNail#()
